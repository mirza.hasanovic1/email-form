// Variables
const sendBtn = document.querySelector("#sendBtn"),
      email = document.getElementById("email"),
      subject = document.getElementById("subject"),
      message = document.getElementById("message"),
      form = document.getElementById("email-form");
      resetBtn = document.getElementById("resetBtn");
// Listeners
eventListeners();
function eventListeners() {
  document.addEventListener("DOMContentLoaded", appInit);
  email.addEventListener("blur", validateFields);
  subject.addEventListener("blur", validateFields);
  message.addEventListener("blur", validateFields);
  resetBtn.addEventListener("click", resetForm);
  form.addEventListener("submit", sendEmail);
}

// Functions
function appInit() {
  sendBtn.disabled = true;
}

function sendEmail(e) {
  e.preventDefault();
  let spinner = document.getElementById("spinner");
  spinner.style.display = "block";
  let sentImg = document.createElement("img");
  sentImg.src = "img/mail.gif";
  sentImg.style.display = "block";
  setTimeout(function() {
    spinner.style.display = "none";
    document.getElementById("loaders").appendChild(sentImg);
    setTimeout(function() {
      form.reset();
      sentImg.remove();
    }, 3000);
  }, 2000);
}

function validateFields() {
  let error;
  validateLength(this);
  if(this.type == "email")
    validateEmail(this);
  error = document.querySelectorAll(".error");
  if(email.value != '' && subject.value != '' && message.value != '') {
    if(error.length == 0) {
      sendBtn.disabled = false;
    }
  }
}

function validateLength(field) {
  if(field.value.length > 0) {
    field.style.borderBottomColor = "green";    
    field.classList.remove("error");
  }
  else{
    field.style.borderBottomColor = "red";    
    field.classList.add("error");
  }
}

function validateEmail(field) {
  if(field.value.indexOf("@") != -1) {
    field.style.borderBottomColor = "green";    
    field.classList.remove("error");
  }
  else{
    field.style.borderBottomColor = "red";    
    field.classList.add("error");
  }
}

function resetForm() {
  form.reset();
}